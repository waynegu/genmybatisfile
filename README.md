#genmybatisfile
### 说明
这是一个dao层自动生成代码工具，可以根据数据库表，自动生成java、dao、mapper.xml文件。&#x2028;

 **注意：建表时，字段要加注释，这样在生成java类的时候字段才有注释** 

### 使用方法

1. 下载源代码。
2. maven构建2个项目，gencode和gen。并下载相应的依赖jar包
  - gencode是插件源代码。代码生成插件的源代码，可以做定制化开发。
  - gen是代码生成工程，用来生成代码，依赖gencode插件。

3. 先 mvn install gencode项目，将插件安装到自己的仓库。让gen可以依赖到。
4. mvn install gen项目。gen项目也要安装下，否则可能无法运行。
5. 修改gen项目的配置文件：generatorConfig.xml，

￼![配置](https://gitee.com/uploads/images/2017/1029/225120_00b4c6e7_1376438.png "配置.png")

6. 在gen项目中，运行插件。mvn mybatis-gen:generate
7. 在第5配置的目录中找生成的文件