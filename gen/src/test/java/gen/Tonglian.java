package gen;

import com.feilong.core.text.MessageFormatUtil;
import com.feilong.io.IOReaderUtil;
import com.feilong.io.LineNumberReaderResolver;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xianze.zxz on 2016/10/30.
 */
public class Tonglian {
    static String format = "{0} {1} NULL COMMENT '{2}',";
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<String,String>();
        map.put("string","varchar(10)");
        map.put("date","DATE");
        map.put("double","DOUBLE");
        final StringBuilder sb = new StringBuilder();
        IOReaderUtil.resolverFile("D:/work/文件/src.txt", new LineNumberReaderResolver() {
            public boolean excute(int lineNumber, String line) {
                String[] split = line.split("\\\\s");
                sb.append(MessageFormatUtil.format(format,split[0],split[1],split[2]));
                return true;
            }
        });
        System.out.println(sb.toString());
    }
}
